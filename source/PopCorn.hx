package;

import flixel.FlxSprite;
import flixel.addons.display.FlxExtendedSprite;
import flixel.FlxObject;
import flixel.util.FlxColor;
import flixel.FlxG;
import flixel.util.FlxTimer;
import flixel.system.FlxSound;
/**
 * ...
 * @author ...
 */
class PopCorn extends FlxExtendedSprite
{
	private var _popSound:FlxSound;
	public function new(X:Float=0, Y:Float=0) 
	{
		super(X, Y);
		loadGraphic(AssetPaths.palomita__png, true, 16, 16);
		this.animation.add("EXPLODE", [1, 2, 3, 4, 5, 6, 7, 8, 9], 16, false);
		
		drag.x = drag.y = 16;
		setSize(16, 16);
		offset.set(0, 0);
		velocity.x = -100;
		velocity.y = -200;
		acceleration.y = 500;
		this.animation.play("EXPLODE");
		
		FlxG.sound.load(AssetPaths.pop__wav).play();

	}
	
	override public function update():Void
	{		
		if (animation.finished)
			this.kill();
		super.update();
	}	
}