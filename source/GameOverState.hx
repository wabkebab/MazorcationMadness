package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxColor;
import flixel.util.FlxDestroyUtil;
import flixel.util.FlxSave;
using flixel.util.FlxSpriteUtil;
import flash.system.System;

class GameOverState extends FlxState
{
	private var _score:Int = 0;			// number of coins we've collected
	private var _win:Bool;				// if we won or lost
	private var _txtTitle:FlxText;		// the title text
	private var _txtMessage:FlxText;	// the final score message text
	private var _sprScore:FlxSprite;	// sprite for a coin icon
	private var _txtScore:FlxText;		// text of the score
	private var _txtHiScore:FlxText;	// text to show the hi-score
	private var _btnMainMenu:FlxButton;	// button to go to main menu
	private var _btnRetry:FlxButton;
	private var _playerDead:Player;
	private var _human:FlxSprite;
	private var _retryText:FlxText;
	private var _exitText:FlxText;
	
	
	/**
	 * Called from PlayState, this will set our win and score variables
	 * @param	Win		true if the player beat the boss, false if they died
	 * @param	Score	the number of coins collected
	 */
	public function new(Win:Bool, Score:Int) 
	{
		super();
		_win = Win;
		_score = Score;
	}
	
	override public function create():Void 
	{
		#if desktop
		// create and add each of our items
		FlxG.mouse.visible = false;
		#end
		_txtTitle = new FlxText(0, 20, 0, _win ? "You Win! \n Enjoy your humanity and your new girlfriend" : "Game Over!", 22);
		_txtTitle.alignment = "center";
		_txtTitle.screenCenter(true, false);
		add(_txtTitle);
		
		_retryText = new FlxText(0, 0, 0, "Press ENTER to Retry", 12);
		_exitText = new FlxText(0, 0, 0, "Press ESC to exit", 12);
		_exitText.screenCenter();
		_exitText.alignment = "center";
		_exitText.y += 170;
		_retryText.screenCenter();
		_retryText.alignment = "center";
		_retryText.y += 150;
		add(_exitText);
		if (_win)
		{
			_human = new FlxSprite(FlxG.width / 2, FlxG.height / 2);
			_human.loadGraphic(AssetPaths.mutil__png, false, 32, 64);
			add(_human);
		}
		else 
		{
			_playerDead = new Player(FlxG.width / 2, FlxG.height / 2);
			_playerDead.acceleration.y = 0;
			_playerDead.setDead();
			add(_playerDead);
			add(_retryText);
		}
		
		super.create();
	}
	
	override public function update():Void
	{
		super.update();
		if (FlxG.keys.justPressed.ONE || FlxG.keys.justPressed.TWO || FlxG.keys.justPressed.ENTER)
			goRetry();			
		else if ((FlxG.keys.pressed.ONE && FlxG.keys.pressed.TWO) || FlxG.keys.justPressed.ESCAPE)
			 System.exit(0);
	}
	
	/**
	 * This function will compare the new score with the saved hi-score. 
	 * If the new score is higher, it will save it as the new hi-score, otherwise, it will return the saved hi-score.
	 * @param	Score	The new score
	 * @return	the hi-score
	 */
	private function checkHiScore(Score:Int):Int
	{
		var _hi:Int = Score;
		var _save:FlxSave = new FlxSave();
		if (_save.bind("flixel-tutorial"))
		{
			if (_save.data.hiscore != null)
			{
				if (_save.data.hiscore > _hi)
				{
					_hi = _save.data.hiscore;
				}
				else
				{
					_save.data.hiscore = _hi;
				}
			}
		}
		_save.close();
		return _hi;
	}
	
	/**
	 * When the user hits the main menu button, it should fade out and then take them back to the MenuState
	 */
	private function goMainMenu():Void
	{
		FlxG.camera.fade(FlxColor.BLACK, .66, false, function() {
			FlxG.switchState(new MenuState());
		});
	}
	
	private function goRetry():Void
	{
		FlxG.camera.fade(FlxColor.BLACK, .25, false, function() {
			FlxG.switchState(new PlayState());
		});
	}
	
	override public function destroy():Void 
	{
		super.destroy();
		
		// clean up all our objects!
		_txtTitle = FlxDestroyUtil.destroy(_txtTitle);
		_txtMessage = FlxDestroyUtil.destroy(_txtMessage);
		_sprScore = FlxDestroyUtil.destroy(_sprScore);
		_txtScore = FlxDestroyUtil.destroy(_txtScore);
		_btnMainMenu = FlxDestroyUtil.destroy(_btnMainMenu);
	}
	
}