package;

import Std;
import flixel.addons.editors.ogmo.FlxOgmoLoader;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.group.FlxTypedGroup;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.ui.FlxButton;
import flixel.util.FlxAngle;
import flixel.util.FlxDestroyUtil;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;
import flixel.addons.display.FlxBackdrop;
import flixel.plugin.TimerManager;
import flixel.util.FlxColor;
import flixel.util.FlxCollision;
import flixel.ui.FlxVirtualPad;
import flash.system.System;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class PlayState extends FlxState
{
	private var _player:Player;
	private var _map:FlxOgmoLoader;
	private var _mWalls:FlxTilemap;
	private var _grpEnemies:FlxTypedGroup<Enemy>;
	private var _background:FlxBackdrop;
	private var _grpCorns:FlxTypedGroup<Corn>;
	private var _hud:HUD;
	private var _portal:FlxSprite;
	private var _counter:Int = 0;
	
	#if mobile
	public static var virtualPad:FlxVirtualPad;
	#end

	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
		#if desktop
		FlxG.mouse.visible = false;
		#end
		_background = new FlxBackdrop(AssetPaths.fondoAnim_01luces__png, 0, 0, false, false);
		_map = new FlxOgmoLoader(AssetPaths.room_001__oel);
		_grpCorns = new FlxTypedGroup<Corn>();
		_portal = new FlxSprite(0, 0);
		_portal.makeGraphic(32, 32, FlxColor.CYAN);
		_mWalls = _map.loadTilemap(AssetPaths.tiles__png, 32, 32, "walls");
		_mWalls.setTileProperties(1, FlxObject.NONE);
		_mWalls.setTileProperties(2, FlxObject.ANY);
		add(_mWalls);
		_grpEnemies = new FlxTypedGroup<Enemy>();
		add(_grpEnemies);
		add(_grpCorns);
		_hud = new HUD();
		add(_hud);
		
		_player = new Player();
		_player.health = 3;
		_map.loadEntities(placeEntities, "entities");
		
		
		add(_portal);
		add(_player);
		
		FlxG.camera.follow(_player, FlxCamera.STYLE_PLATFORMER, 1);
		
		#if mobile
		virtualPad = new FlxVirtualPad(FULL, NONE);
		add(virtualPad);
		#end
		
		super.create();			
	}
	
	private function placeEntities(entityName:String, entityData:Xml):Void
	{
		var x:Int = Std.parseInt(entityData.get("x"));
		var y:Int = Std.parseInt(entityData.get("y"));
		
		if (entityName == "Player")
		{
			_player.x = x;
			_player.y = y;
		}
		
		else if (entityName == "Enemy")
		{
			_grpEnemies.add(new Enemy(x + 4, y, Std.parseInt(entityData.get("etype"))));
		}
		else if (entityName == "Portal")
		{
			_portal.x = x;
			_portal.y = y;
		}
	}

	public function shoot():Void
	{
		if (_player.facing == FlxObject.LEFT)
			_grpCorns.add(new Corn(_player.x + _player.width / 2, _player.y + 14, _player.facing));
		else 
			_grpCorns.add(new Corn(_player.x + _player.width / 2, _player.y + 14, _player.facing));
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
		_background.destroy();
		_grpCorns = FlxDestroyUtil.destroy(_grpCorns);
		_grpEnemies = FlxDestroyUtil.destroy(_grpEnemies);
		_player = FlxDestroyUtil.destroy(_player);
		_portal = FlxDestroyUtil.destroy(_portal);
		_hud = FlxDestroyUtil.destroy(_hud);
		#if mobile
		virtualPad = FlxDestroyUtil.destroy(virtualPad);
		#end
	}
	
	/**
	 * Function that is called once every frame.
	 */
	
	override public function update():Void
	{
		super.update();
		var space:Bool = false;
		#if desktop
		space = FlxG.keys.anyJustPressed(["SPACE", "X", "Z"]);
		#end
		if (space) 
			shoot();
		if (!_player.isHitable)
		{
			_counter ++;
			if (_counter >= 60)
			{
				_player.isHitable = true;
				_counter = 0;
			}
		}
		
		if (FlxG.keys.pressed.ONE && FlxG.keys.pressed.TWO)
			 System.exit(0);
		
		FlxG.collide(_grpCorns, _grpEnemies, cornTouchEnemy);
		FlxG.collide(_player, _mWalls);
		FlxG.collide(_grpEnemies, _mWalls);
		FlxG.collide(_grpCorns, _mWalls);		

		_grpEnemies.forEachAlive(checkEnemyCollision);	
		
		if (FlxG.overlap(_player, _portal))
			FlxG.switchState(new BossSceneFight());
	}	
	
	private function checkEnemyCollision(e:Enemy):Void
	{
		if(FlxCollision.pixelPerfectCheck(_player, e, 1))
		{
			FlxG.collide(_player, e);
			playerInjury(_player, e);
		}
	}
	
	private function checkEnemyVision(e:Enemy):Void
	{
		if (e.exists)
		{
			try
			{
				if (e.etype == 1)
				{
					if (_mWalls.ray(e.getMidpoint(), _player.getMidpoint(), 10))
					{
						e.seesPlayer = true;
						e.playerPos.copyFrom(_player.getMidpoint());
					}
					else
						e.seesPlayer = false;
				}
			}
			catch(msg:String)
			{
				trace(msg);
			}		
		}		
	}
	
	private function cornTouchEnemy(C:Corn, E:Enemy):Void
	{
		if (C.alive && C.exists && E.alive && E.exists)
		{
			add(new PopCorn(C.x, C.y));
			C.kill();
			E.kill();
			
		}
	}
	
	override public function draw():Void
	{
		_background.draw();
		super.draw();
	}
	
	private function playerInjury(P:Player, E:Enemy):Void
	{
		if (P.alive && P.exists && E.alive && E.exists && P.isHitable)
		{
			if (P.isHitable)
			{
				P.velocity.x = -200;
				P.velocity.y = -200;
				FlxG.camera.shake(0.0025, 0.1);
				P.setHitAnimation();
				P.health--;
				P.isHitable = false;
				_hud.updateHUD(Std.int(P.health));
				FlxG.camera.flash(0xff0000, 0.25);
			}
		}
	}
	
}
