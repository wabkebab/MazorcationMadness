package;

import flixel.addons.editors.tiled.TiledMap;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxAngle;
import flixel.util.FlxPoint;
import flixel.util.FlxRandom;
import flixel.util.FlxVelocity;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.tile.FlxTilemap;

class Enemy extends FlxSprite 
{
	public var speed:Float = 80;
	public var etype(default, null):Int;
	private var _brain:FSM;
	private var _idleTmr:Float;
	private var _moveDir:Float;
	public var seesPlayer:Bool = false;
	public var playerPos(default, null):FlxPoint;
	private var gravity:Int = 0;
	
	private var tilemap:FlxTilemap;
	
	public function new(X:Float=0, Y:Float=0, EType:Int) 
	{
		super(X, Y);
		etype = EType;
		if (etype == 0)
		{
			loadGraphic(AssetPaths.enemy_1__png, true, 72, 64);
			acceleration.y = 500;
			setSize(72, 64);
			offset.set(15,0);
		}
		else if (etype == 1)
		{
			loadGraphic(AssetPaths.enemy_2__png, true, 32, 32);
			setSize(32, 32);
			offset.set(15,0);
		}
		setFacingFlip(FlxObject.LEFT, false, false);
		setFacingFlip(FlxObject.RIGHT, true, false);
		if(etype == 0)
			animation.add("elr", [1, 2, 3, 3, 5], 6, true);
		else if(etype == 1)
			animation.add("elr", [1, 2, 3, 4], 6, true);
		drag.x = drag.y = 10;
		_brain = new FSM(idle);
		_idleTmr = 0;
		playerPos = FlxPoint.get();
	}
	
	override public function update():Void 
	{
		
		_brain.update();
		super.update();
	}
	
	public function idle():Void
	{
		if (seesPlayer)
		{
			_brain.activeState = chase;
		}
		else if (_idleTmr <= 0)
		{
			if (FlxRandom.chanceRoll(1))
			{
				_moveDir = -1;
				velocity.x = velocity.y = 0;
			}
			else
			{
				_moveDir = FlxRandom.intRanged(0, 8) * 45;
				FlxAngle.rotatePoint(speed * .5, 0, 0, 0, _moveDir, velocity);
				
			}
			_idleTmr = FlxRandom.intRanged(1, 4);			
		}
		else
			_idleTmr -= FlxG.elapsed;
		
	}
	
	public function chase():Void
	{
		if (!seesPlayer)
		{
			_brain.activeState = idle;
		}
		else
		{
			FlxVelocity.moveTowardsPoint(this, playerPos, speed);
		}
	}
	
	
	override public function draw():Void 
	{
		if (velocity.x != 0 || velocity.y != 0)
		{
			
			if (Math.abs(velocity.x) > Math.abs(velocity.y))
			{
				if (velocity.x < 0)
					facing = FlxObject.LEFT;
				else
					facing = FlxObject.RIGHT;
			}
			else
			{
				if (velocity.y < 0)
					facing = FlxObject.UP;
				else
					facing = FlxObject.DOWN;
			}
			
			if (facing == FlxObject.RIGHT || facing == FlxObject.LEFT)
				animation.play("elr");
		}
			
		super.draw();
	}
	
	override public function kill():Void 
	{
		alive = false;
		FlxTween.tween(this, { alpha:0, y:y - 16 }, .66, { type:FlxTween.ONESHOT, ease:FlxEase.circOut, complete:finishKill } );
	}
	
	private function finishKill(T:FlxTween):Void
	{
		exists = false;
	}
	
}