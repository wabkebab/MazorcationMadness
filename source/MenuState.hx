package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.util.FlxDestroyUtil;
using flixel.util.FlxSpriteUtil; 
import flixel.util.FlxColor;
import flixel.addons.display.FlxBackdrop;
import flash.system.System;

/**
 * A FlxState which can be used for the game's menu.
 */
class MenuState extends FlxState
{
	private var _txtTitle:FlxText;
	private var _btnOptions:FlxButton;
	private var _text:FlxText;
	private var _btnPlay:FlxButton;
	private var _pressToPlay:FlxText;
	
	private var _background:FlxBackdrop;
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	 
	override public function create():Void
	{
		#if desktop
		FlxG.mouse.visible = false;
		#end
		_text = new FlxText(0, 0 , 450, "Direction keys and A-D to move\nUP and W to jump or double jump\nSPACE to shoot corns", 12);
		_pressToPlay = new FlxText(0, 0, 0, "Press ENTER to play.\nPress ESC to exit.", 12);
		_background = new FlxBackdrop(AssetPaths.artaburuDiscoMadness__png, 0, 0, false, false);
		add(_text);
		_text.screenCenter();
		_text.y -= 100;
		_text.x += 125;
		//_btnPlay = new FlxButton(0, 0, "Play", clickPlay);
		_pressToPlay.screenCenter();
		_pressToPlay.y += 75;
		add(_pressToPlay);
		//_btnPlay.screenCenter();
		
		_btnOptions = new FlxButton(10, 20, "Options", clickOptions);
		_btnOptions.screenCenter();
		_btnOptions.y += 32;
		//add(_btnOptions);
		
		super.create();	
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
		_background.destroy();
		_txtTitle = FlxDestroyUtil.destroy(_txtTitle);
		_btnPlay = FlxDestroyUtil.destroy(_btnPlay);
		_btnOptions = FlxDestroyUtil.destroy(_btnOptions);
	}

	private function clickPlay():Void
	{
		FlxG.camera.fade(FlxColor.BLACK, .66, false, function() {
			FlxG.switchState(new PlayState());
		});		
	}
	
	private function clickOptions():Void
	{
		FlxG.switchState(new OptionState());
	}
	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		super.update();
		if (FlxG.keys.justPressed.ONE || FlxG.keys.justPressed.TWO || FlxG.keys.justPressed.ENTER)
			clickPlay();			
		else if ((FlxG.keys.pressed.ONE && FlxG.keys.pressed.TWO) || FlxG.keys.justPressed.ESCAPE)
			 System.exit(0);
	}	
	
	override public function draw():Void 
	{
		_background.draw();
		super.draw();
	}
}