package ;

import flixel.addons.display.FlxExtendedSprite;
import flixel.FlxG;
/**
 * ...
 * @author ...
 */
class Platforms extends FlxExtendedSprite
{
	private var deltaDistPerFrame:Float;
	private var origin_Y:Float;
	private var end_Y:Float;
	
	public function new(X:Float = 0, Y:Float = 0, Y_:Float = 0, fullLoopTime:Float = 0 ) 
	{
		super(X, Y);
		origin_Y = Y;
		end_Y = Y_;
		deltaDistPerFrame = FlxG.elapsed * 2 * (end_Y -origin_Y) / fullLoopTime;
		loadGraphic(AssetPaths.piedra_002__png, false, 64, 32);
		setSize(64, 32);
		offset.set(0, 0);		
		
	}
	
	override public function update():Void
	{
		this.y += deltaDistPerFrame;
		
		if (this.y <= origin_Y || this.y >= end_Y) 
			deltaDistPerFrame *= -1;
	}
	
	
}