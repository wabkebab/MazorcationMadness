package ;

import flixel.addons.display.FlxExtendedSprite;
import flixel.util.FlxColor;
import flixel.FlxG;
import flixel.util.FlxAngle;
import flixel.FlxObject;
import flixel.input.keyboard.FlxKeyboard;
import flash.system.System;

/**
 * ...
 * @author ...
 */
class Player extends FlxExtendedSprite
{
	public var isHitable:Bool = true;
	private var _jumpKeys:Array<String>;
	private var _jumpTime:Float = -1;
	private static inline var JUMP_SPEED:Int = 550;
	private static inline var RUN_SPEED:Int = 250;
	
	public function new(X:Float=0, Y:Float=0) 
	{
		super(X, Y);
		health = 3;
		loadGraphic(AssetPaths.maiz_final__png, true, 64, 64);
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
		animation.add("lr", [ 3, 2, 4, 1], 4, false);
		animation.add("up", [3], 4, false);
		animation.add("dwn", [4], 4, false);
		animation.add("dead", [6], 4, false);
		animation.add("hurt", [5], 1, false);
		drag.x = drag.y = 1600;
		setSize(32, 64);
		offset.set(15, 0);		
		this.acceleration.y = 1500;
		_jumpKeys = ["UP", "C", "W", "V"];
		maxVelocity.set(RUN_SPEED, JUMP_SPEED);
	}
	
	public function movement():Void
	{
		var up:Bool = false;
		var left:Bool = false;
		var right:Bool = false;
		
		#if desktop
		if (alive)
		{
			//up = FlxG.keys.anyPressed(["UP", "W"]);
			left = FlxG.keys.anyPressed(["LEFT", "A"]);
			right = FlxG.keys.anyPressed(["RIGHT", "D"]);	
		}
		#end	
		
		if (left && right) //disables move if both were pushed
			left = right = false;

		if (this.touching == FlxObject.FLOOR && up) //this makes the jump
			this.velocity.y = -JUMP_SPEED;

		if (right) {
			this.velocity.x = RUN_SPEED;
			this.facing = FlxObject.RIGHT;
		}
		if (left) {
			this.velocity.x = -RUN_SPEED;
			this.facing = FlxObject.LEFT;
		}
		
		// DEATH ///////
		
		//by downfall
		if (y >= FlxG.height) {
			FlxG.switchState(new GameOverState(false, 0));
		}
		
		//by extras
		
		
		//////////////////////////////////////////
		if 	 ((this.velocity.x != 0 || this.velocity.y > 0) && (this.touching == FlxObject.FLOOR) )	this.animation.play("lr");
		else if (this.velocity.y > 0) this.animation.play("dwn");
		else if (this.velocity.y < 0) this.animation.play("up"); //movement animations
	}
	
	private function jump():Void 
	{
		if (FlxG.keys.anyJustPressed(_jumpKeys))
			if (this.touching == FlxObject.FLOOR) // Only allow two jumps
				_jumpTime = 0;
		
		// You can also use space or any other key you want
		if ((FlxG.keys.anyPressed(_jumpKeys)) && (_jumpTime >= 0)) 
		{
			_jumpTime += FlxG.elapsed;
			
			// You can't jump for more than 0.25 seconds
			if (_jumpTime > 0.25)
				_jumpTime = -1;
			else if (_jumpTime > 0)
				velocity.y = - 0.6 * maxVelocity.y;
		}	
	}	
		
	override public function update():Void 
	{		
		movement();	
		jump();
		if (health <= 0) 
			FlxG.switchState(new GameOverState(false, 0));
		super.update();
	}

	public function setDead():Void
	{
		this.animation.play("dead");
		this.alive = false;
	}
	
	public function setHitAnimation():Void
	{
		this.animation.play("hurt");
	}
}