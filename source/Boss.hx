package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxAngle;
import flixel.util.FlxPoint;
import flixel.util.FlxRandom;
import flixel.util.FlxVelocity;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxColor;


class Boss extends FlxSprite 
{
	public var speed:Float = 80;
	public var etype(default, null):Int;
	private var _idleTmr:Float;
	private var _moveDir:Float;
	public var seesPlayer:Bool = false;
	public var playerPos(default, null):FlxPoint;
	private var gravity:Int = 0;
	
	public function new(X:Float=0, Y:Float=0) 
	{
		super(X, Y);
		loadGraphic(AssetPaths.berenjena__png, true, 64, 64);
		setSize(64, 64);
		offset.set(15,0);
		health = 64;
		setFacingFlip(FlxObject.LEFT, false, false);
		setFacingFlip(FlxObject.RIGHT, true, false);
		animation.add("elr", [0, 1, 1, 2, 4, 5], 6, true);
		_idleTmr = 0;
		playerPos = FlxPoint.get();
		alive = true;
		exists = true;
		velocity.y = -200;
		facing = FlxObject.LEFT;
		
		
	}
	
	override public function update():Void 
	{		
		move();
		super.update();
	}
	
	private function move():Void
	{
		if ((this.y < 48 && velocity.y < 0) || (this.y > FlxG.height - (64 + 72) && velocity.y > 0))
			velocity.y *= -1; 
	}
	
	override public function draw():Void 
	{
		if (velocity.x != 0 || velocity.y != 0)
		{
			
			if (Math.abs(velocity.x) > Math.abs(velocity.y))
			{
				if (velocity.x < 0)
					facing = FlxObject.LEFT;
				else
					facing = FlxObject.RIGHT;
			}
			else
			{
				if (velocity.y < 0)
					facing = FlxObject.UP;
				else
					facing = FlxObject.DOWN;
			}
			
			if (facing == FlxObject.RIGHT || facing == FlxObject.LEFT)
				animation.play("elr");
		}
			
		super.draw();
	}
	
	override public function kill():Void 
	{
		alive = false;
		FlxTween.tween(this, { alpha:0, y:y - 16 }, 2, { type:FlxTween.ONESHOT, ease:FlxEase.circOut, complete:finishKill } );
	}
	
	private function finishKill(T:FlxTween):Void
	{
		exists = false;
	}
	
}