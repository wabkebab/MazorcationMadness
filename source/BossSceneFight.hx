package;

import Std;
import flixel.addons.editors.ogmo.FlxOgmoLoader;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.group.FlxTypedGroup;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.ui.FlxButton;
import flixel.util.FlxAngle;
import flixel.util.FlxDestroyUtil;
import flixel.util.FlxMath;
import flixel.util.FlxPoint;
import flixel.addons.display.FlxBackdrop;
import flixel.plugin.TimerManager;
import flixel.util.FlxColor;
import flixel.util.FlxCollision;
import flixel.ui.FlxVirtualPad;
import flash.system.System;
import flixel.util.FlxRandom;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class BossSceneFight extends FlxState
{
	private var _player:Player;
	private var _map:FlxOgmoLoader;
	private var _mWalls:FlxTilemap;
	private var _boss:Boss;
	private var _background:FlxBackdrop;
	private var _grpCorns:FlxTypedGroup<Corn>;
	private var _grpEnemies: FlxTypedGroup<Enemy>;
	private var _hud:HUD;
	private var _portal:FlxSprite;	
	private var _counter:Int = 0;
	private var _countdound:Int = 60;
	private var _txtCountDown:FlxText;
	private var life:FlxText;
	private var frameContuer:Int = 0;
	
	#if mobile
	public static var virtualPad:FlxVirtualPad;
	#end
	
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	
	
	override public function create():Void
	{
		#if desktop
		FlxG.mouse.visible = false;
		#end
		_background = new FlxBackdrop(AssetPaths.fondoAnim_01luces__png, 0, 0, false, false);
		_txtCountDown = new FlxText(FlxG.width / 2, 48, 450, "", 12);
		_map = new FlxOgmoLoader(AssetPaths.room_002__oel); //CAMBIAR CON LA ZONA FINAL
		_grpCorns = new FlxTypedGroup<Corn>();
		_grpEnemies = new FlxTypedGroup<Enemy>();
		_mWalls = _map.loadTilemap(AssetPaths.tiles__png, 32, 32, "walls");
		_mWalls.setTileProperties(1, FlxObject.NONE);
		_mWalls.setTileProperties(2, FlxObject.ANY);
		add(_mWalls);
		_boss = new Boss(0, 0);
		add(_boss);
		add(_grpCorns);
		_hud = new HUD();
		add(_hud);
		add(_txtCountDown);
		add(_grpEnemies);
		
		_player = new Player();
		_player.health = 3;
		_map.loadEntities(placeEntities, "entities");
		
		add(_player);
		
		life = new FlxText(50, 50, 100, "", 12);
		
		add(life);
		
		FlxG.camera.follow(_player, FlxCamera.STYLE_SCREEN_BY_SCREEN, 1);  //QUIZAS HEMEN HOBE DA KAMARA ZENTRATUTA IzATEA
		
		#if mobile
		virtualPad = new FlxVirtualPad(FULL,ActionMode.A);
		add(virtualPad);
		#end
		
		super.create();			
	}
	
	private function placeEntities(entityName:String, entityData:Xml):Void
	{
		var x:Int = Std.parseInt(entityData.get("x"));
		var y:Int = Std.parseInt(entityData.get("y"));
		
		if (entityName == "Player")
		{
			_player.x = x;
			_player.y = y;
		}
		
		else if (entityName == "Enemy")
		{
			_boss.x = x;
			_boss.y = y;
		}
	}

	public function shoot():Void
	{
		if (_player.facing == FlxObject.LEFT)
			_grpCorns.add(new Corn(_player.x + _player.width / 2, _player.y + 14, _player.facing));
		else 
			_grpCorns.add(new Corn(_player.x + _player.width / 2, _player.y + 14, _player.facing));
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
		_background.destroy();
		_txtCountDown.destroy();
		#if mobile
		virtualPad = FlxDestroyUtil.destroy(virtualPad);
		#end
	}
	
	/**
	 * Function that is called once every frame.
	 */
	
	override public function update():Void
	{
		super.update();
		var space:Bool = false;
		#if desktop
		space = FlxG.keys.justPressed.SPACE;
		#end
		if (space) 
			shoot();
		
		if (!_player.isHitable)
		{
			_counter ++;
			if (_counter >= 60)
			{
				_player.isHitable = true;
				_counter = 0;
			}
		}
		
		life.x = _boss.x;
		life.y = _boss.y - 32;
		
		if (frameContuer++ >= 60)
		{
			_countdound--;
			frameContuer = 0;
		}
		
		life.text = Std.string(_boss.health);
		
		if (FlxG.keys.pressed.ONE && FlxG.keys.pressed.TWO)
			 System.exit(0);
		
		checkEnemyCollision(_boss);
		FlxG.overlap(_grpCorns, _boss, cornTouchEnemy);
		FlxG.collide(_player, _mWalls);
		FlxG.collide(_boss, _mWalls);
		FlxG.collide(_grpCorns, _mWalls);
		
		if (FlxG.overlap(_grpEnemies, _player))
		{
			if (_player.isHitable)
			{
				_player.velocity.x = -200;
				_player.velocity.y = -200;
				FlxG.camera.shake(0.0025, 0.1);
				_player.setHitAnimation();
				_player.health--;
				_player.isHitable = false;
				_hud.updateHUD(Std.int(_player.health));
				FlxG.camera.flash(0xff0000, 0.25);
			}
		}
		
		_txtCountDown.text = Std.string(_countdound);
		
		if (_player.health == 0 || _countdound == 0)
			FlxG.switchState(new GameOverState(false, 0));
		if (_boss.health == 0)
			FlxG.switchState(new GameOverState(true, 0));
		
		if (FlxRandom.chanceRoll(0.1))
			_grpEnemies.add(new Enemy(FlxRandom.intRanged(120, FlxG.width - 120), FlxRandom.intRanged(64, FlxG.height - 32), 1));
	}	
	
	private function checkEnemyCollision(e:Boss):Void
	{
		if(FlxCollision.pixelPerfectCheck(_player, e, 1))
		{
			FlxG.collide(_player, e);
			playerInjury(_player, e);
		}
	}
	
	private function cornTouchEnemy(C:Corn, E:Boss):Void
	{
		if (C.alive && C.exists && E.alive && E.exists)
		{
			add(new PopCorn(C.x, C.y));
			C.kill();
			E.hurt(2);
		}
	}

	private function checkEnemyVision(e:Boss):Void
	{
		if (e.exists)
		{
			try
			{
				if (_mWalls.ray(e.getMidpoint(), _player.getMidpoint(), 10))
				{
					e.seesPlayer = true;
					e.playerPos.copyFrom(_player.getMidpoint());
				}
				else
					e.seesPlayer = false;
			}
			catch(msg:String)
			{
				trace(msg);
			}		
		}		
	}
	
	override public function draw():Void
	{
		_background.draw();
		super.draw();
	}
	
	private function playerInjury(P:Player, E:Boss):Void
	{
		if (P.alive && P.exists && E.alive && E.exists)
		{
			if (P.isHitable)
			{
				P.velocity.x = -200;
				P.velocity.y = -200;
				FlxG.camera.shake(0.0025, 0.1);
				P.setHitAnimation();
				P.health--;
				P.isHitable = false;
				_hud.updateHUD(Std.int(P.health));
			}
		}
	}
}
