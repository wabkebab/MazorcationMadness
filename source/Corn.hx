package ;

import flixel.addons.display.FlxExtendedSprite;
import flixel.FlxObject;
import flixel.util.FlxColor;
import flixel.FlxG;
import flixel.util.FlxTimer;
/**
 * ...
 * @author ...
 */
class Corn extends FlxExtendedSprite
{
	private var vel:Float = 100;
	private var time:Int = 0;
	public function new(X:Float=0, Y:Float=0,fc:Int) 
	{
		super(X , Y);
		loadGraphic(AssetPaths.palomita__png, true, 16, 16);
		animation.add("EXPLODE", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 4, false);
		time = 0;
		drag.x = drag.y = 16;
		setSize(16, 16);
		offset.set(0, 0);
		if (fc == FlxObject.LEFT) {  vel = -1000; }
		else vel = 1000;		
	}
	
	
	override public function update()
	{
		this.x += vel * FlxG.elapsed;	
		this.time++;
		if (this.time > 30) 
			this.kill();
	}
	
	override public function kill():Void 
	{
		//animation.play("EXPLODE");
		vel = 0;
		alive = false;
		exists = false;
	}
	
}